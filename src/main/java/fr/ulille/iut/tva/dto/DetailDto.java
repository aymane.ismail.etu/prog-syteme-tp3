package fr.ulille.iut.tva.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DetailDto {
	
	double montantTotal;
	double montantTva;
	double somme;
	String tauxLabel;
	double tauxValue;
	
	public DetailDto() {}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}

	public double getMontantTva() {
		return montantTva;
	}

	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public String getTauxLabel() {
		return tauxLabel;
	}

	public void setTauxLabel(String tauxLabel) {
		this.tauxLabel = tauxLabel;
	}

	public double getTauxValue() {
		return tauxValue;
	}

	public void setTauxValue(double tauxValue) {
		this.tauxValue = tauxValue;
	}

	public DetailDto(double montantTotal, double montantTva, double somme, String tauxLabel, double tauxValue) {
		super();
		this.montantTotal = montantTotal;
		this.montantTva = montantTva;
		this.somme = somme;
		this.tauxLabel = tauxLabel;
		this.tauxValue = tauxValue;
	}
}