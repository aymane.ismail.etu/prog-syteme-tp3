package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.DetailDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */

@Path("tva")
public class TvaRessource {

	private CalculTva calculTva = new CalculTva();


	@GET
	@Path("tauxpardefaut")
	public double getValeurTauxParDefaut() {
		return TauxTva.NORMAL.taux;
	}

	@GET
	@Path("valeur/{niveauTva}")
	public float getValeurTaux(@PathParam("niveauTva") String niveau) {
		try {
			return (float) TauxTva.valueOf(niveau.toUpperCase()).taux; 
		}
		catch ( Exception ex ) {
			throw new NiveauTvaInexistantException();
		}
	}



	@GET
	@Path("{niveauTva}")
	// PARAMETRE SOMME : http://localhost:8080/api/v1/tva/reduit?somme=100
	public double getMontantTotal(@QueryParam("somme") double somme, @PathParam("niveauTva") String niveau ) {

		try {
			return calculTva.calculerMontant(TauxTva.valueOf(niveau.toUpperCase()), somme);
		}
		catch ( Exception ex ) {
			
			throw new NiveauTvaInexistantException();
			
		}
		
	}

	 @GET
	    @Path("lestaux")
	    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	    public List<InfoTauxDto> getInfoTaux() {
	      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
	      for ( TauxTva t : TauxTva.values() ) {
	        result.add(new InfoTauxDto(t.name(), t.taux));
	      }
	      return result;
	    }
	 
	   @GET
	    @Path("details/{niveauTva}")
	    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	    public DetailDto getDetail (@PathParam("niveauTva") String niveau, @QueryParam("somme") Double somme) {
			return new DetailDto(getMontantTotal(somme, niveau),getValeurTaux(niveau),somme,TauxTva.valueOf(niveau.toUpperCase()).toString(),getValeurTaux(niveau));
	    }


}
